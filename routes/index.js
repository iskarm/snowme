var express = require('express');
var router = express.Router();

var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/test');

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
  // we're connected!
});









/* GET home page. */
router.get('/landing', function(req, res, next) {
  res.render('index', { title: 'Snowme' });
});

router.get('/crawler', function(req, res, next) {
  res.render('crawler.php', { title: 'Snowme' });
});


router.get('/testpage', function(req, res, next) {
  res.render('testpage', { title: 'Snowme' });
});





module.exports = router;
